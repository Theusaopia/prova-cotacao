package com.example.provacotacao;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.provacotacao.entities.Cotacao;

public class ConverterActivity extends AppCompatActivity {
    Spinner primeiro_spinner;
    Spinner segundo_spinner;

    TextView cotacaoInfo;

    ApiCotacao servico;

    ServiceConnection conexao = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            servico = ((ApiCotacao.BinderPersonalizado) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            servico = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent it = new Intent(getApplicationContext(), ApiCotacao.class);
        bindService(it, conexao, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(conexao);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        cotacaoInfo = (TextView) findViewById(R.id.cotacaoInfo);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.array_moedas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        primeiro_spinner = (Spinner) findViewById(R.id.combo_moeda_um);
        primeiro_spinner.setAdapter(adapter);

        segundo_spinner = (Spinner) findViewById(R.id.combo_moeda_dois);
        segundo_spinner.setAdapter(adapter);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void converterDuasMoedas(View view) {
        String moeda1 = primeiro_spinner.getSelectedItem().toString();
        String moeda2 = segundo_spinner.getSelectedItem().toString();

        Cotacao cotacao = servico.buscarCotacao(moeda1, moeda2);

        cotacaoInfo.setText(gerarTextoConversaoDuasMoedas(cotacao));
    }

    public String gerarTextoConversaoDuasMoedas(Cotacao cotacao) {
        StringBuilder sb = new StringBuilder();
        sb.append("Conversão - " + cotacao.getName());
        sb.append("\n");
        sb.append("1 " + cotacao.getCode() + " = " + cotacao.getHigh() + " " + cotacao.getCodein());
        return sb.toString();
    }
}
