package com.example.provacotacao;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.provacotacao.entities.Cotacao;

public class PesquisaActivity extends AppCompatActivity {
    Spinner spinner;
    EditText dias;
    TextView pesquisa;

    ApiCotacao servico;

    ServiceConnection conexao = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            servico = ((ApiCotacao.BinderPersonalizado) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            servico = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent it = new Intent(getApplicationContext(), ApiCotacao.class);
        bindService(it, conexao, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(conexao);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periodo);

        spinner = (Spinner) findViewById(R.id.combo_moedas);
        dias =  (EditText) findViewById(R.id.edQuantidade);
        pesquisa = (TextView) findViewById(R.id.tvPesquisa);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.array_moedas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void pesquisar(View view) {
        String moeda = spinner.getSelectedItem().toString();
        String diasPesquisa = dias.getText().toString();

        Cotacao[] cotacoes = servico.pesquisaCotacao(moeda, diasPesquisa);

        pesquisa.setText(gerarTextoPesquisa(cotacoes, cotacoes[0].getName(), cotacoes[0].getCode()));
    }

    public String gerarTextoPesquisa(Cotacao[] lista, String nome, String moeda) {
        StringBuilder sb = new StringBuilder();
        sb.append(nome + "\n");
        sb.append("------------- \n");

        for(int i = 0; i < lista.length; i++) {
            sb.append("1 BRL = " + lista[i].getHigh() + " " + moeda + "\n");
        }

        return sb.toString();
    }
}
