package com.example.provacotacao;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.example.provacotacao.entities.Cotacao;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class ApiCotacao extends Service {
    BinderPersonalizado binder;

    public ApiCotacao() {
        binder = new BinderPersonalizado();
    }

    public class BinderPersonalizado extends Binder {
        public ApiCotacao getService() {
            return ApiCotacao.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public Cotacao buscarCotacao(String primeiraMoeda, String segundaMoeda) {
        try {
            URL url = new URL("https://economia.awesomeapi.com.br/last/" + primeiraMoeda + "-" + segundaMoeda);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.connect();

            if (conn.getResponseCode() == 200) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String resp = "";
                String linha = "";

                do {
                    linha = reader.readLine();
                    if (linha != null) {
                        resp += linha;
                    }
                } while (linha != null);

                Gson gson = new GsonBuilder().create();

                resp = resp.substring(10, resp.length() - 1);

                Cotacao c = gson.fromJson(resp, Cotacao.class);
                return c;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            return null;
        }
        return null;
    }

    public List<Cotacao> cotacaoToReal(List<String> moedas) {
        List<Cotacao> cotacoes = new ArrayList<>();

        for (int i = 0; i < moedas.size(); i++) {
            cotacoes.add(buscarCotacao(moedas.get(i), "BRL"));
        }

        return cotacoes;
    }

    public Cotacao[] pesquisaCotacao(String moeda, String dias) {
        try {
            URL url = new URL("https://economia.awesomeapi.com.br/json/daily/" + moeda + "-BRL/" + dias);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.connect();

            if (conn.getResponseCode() == 200) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String resp = "";
                String linha = "";

                do {
                    linha = reader.readLine();
                    if (linha != null) {
                        resp += linha;
                    }
                } while (linha != null);

                Gson gson = new GsonBuilder().create();

                Cotacao[] cotacoes = gson.fromJson(resp, Cotacao[].class);

                return cotacoes;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println(ex.getMessage());
            return null;
        }
        return null;
    }
}