package com.example.provacotacao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void converter(View view) {
        Intent it = new Intent(this, ConverterActivity.class);
        startActivity(it);
    }

    public void reais(View view) {
        Intent it = new Intent(this, ListaActivity.class);
        startActivity(it);
    }

    public void periodo(View view) {
        Intent it = new Intent(this, PesquisaActivity.class);
        startActivity(it);
    }
}