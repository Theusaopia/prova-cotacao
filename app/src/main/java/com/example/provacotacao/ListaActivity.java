package com.example.provacotacao;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.provacotacao.entities.Cotacao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListaActivity extends AppCompatActivity {
    ListView listaMoedas;
    ArrayAdapter adapter;

    List<String> moedas = new ArrayList(Arrays.asList("USD", "CAD", "EUR", "GBP", "ARS", "BTC", "LTC", "JPY", "CHF", "ILS"));

    ApiCotacao servico;

    ServiceConnection conexao = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            servico = ((ApiCotacao.BinderPersonalizado) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            servico = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent it = new Intent(getApplicationContext(), ApiCotacao.class);
        bindService(it, conexao, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(conexao);
        super.onStop();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        listaMoedas = (ListView) findViewById(R.id.listaMoedas);

        adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_multiple_choice, moedas);
        listaMoedas.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listaMoedas.setAdapter(adapter);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void converterReais(View view) {
        List<String> auxiliar = new ArrayList<>();

        SparseBooleanArray checked = listaMoedas.getCheckedItemPositions();

        for(int i = 0; i < checked.size(); i++) {
            if(checked.valueAt(i)) {
                String moeda = moedas.get(checked.keyAt(i));
                int position = moedas.indexOf(moeda);

                if(position != -1) {
                    auxiliar.add(moeda);
                }
            }
        }

        Intent i = new Intent(this, ResultadoActivity.class);
        i.putExtra("resultado", gerarTextoConversao(servico.cotacaoToReal(auxiliar)));
        startActivity(i);
    }

    public String gerarTextoConversao(List<Cotacao> cotacoes) {
        StringBuilder sb = new StringBuilder();

        for(Cotacao cot : cotacoes) {
            sb.append(cot.getName());
            sb.append("\n");
            sb.append("1 " + cot.getCode() + " = " + cot.getHigh() + " BRL");
            sb.append("\n -------------------------------------------------- \n");
        }

        return sb.toString();
    }
}
