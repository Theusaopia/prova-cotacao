package com.example.provacotacao;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ResultadoActivity extends AppCompatActivity {
    TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_conversao_reais);

        resultado = (TextView) findViewById(R.id.tvConversao);

        Bundle extras = getIntent().getExtras();

        if(extras != null) {
            String result = extras.getString("resultado");
            resultado.setText(result);
        }
    }
}
